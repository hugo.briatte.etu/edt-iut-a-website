# Projet emploi du temps web client

## Présentation de la problématique

A ce jour, il existe 4 moyens principaux pour aller voir l'emploi du temps :

* Aller sur le site original : edt-iut.univ-lille.fr/hp/etudiant
* Par un bot discord (hébergé sur le discord de la promotion) : git-iut.univ-lille1.fr/giregethugo/botdiscord
* Par un bot messenger : www.facebook.com/botiuta
* Par les emploi du temps affiché régulièrement dans les couloirs

Il y'a cependant plusieurs problèmes :

* Les emploi du temps papier nécessitent de descendre régulièrement a l'étage 1 et ne sont pas accesible en dehors de l'IUT
* Le bot discord et messenger sont plus limités et nécéssitent une authentification à un service tierce que certaine personnes ne possèdent pas forcément
* Le site original demande une authentification et est peu ergonomique lorsque l'on shouaite voir rapidement l'emploi du temps de n'importe quel groupe

Ainsi est venu l'idée de réaliser une application web pour que n'importe qui puisse consulter efficacement et rapidement les emplois du temps.

Une REST API permettant de consulter rapidement les données sur les emploi du temps est disponible à l'adresse : api.hugobriatte.fr/cour (Git du projet : git-iut.univ-lille1.fr/briatteh/IUTAPIEDT). Le but du jeu du projet est de s'en servir afin de pouvoir accéder le plus facilement et sans limite à ces données.

Le site actuellement en developpement sur master est disponible a l'adresse : edt.hugobriatte.fr

## Organisation et participation au projet

Ce projet est accesible et éditable par toute personne possédant un compte sur ce git (c'est à dire tout les étudiants de l'IUT A de Lille en Informatique, S1 à S4). Vous pouvez tous y participer pour l'améliorer. Je vous invite vraiment a le faire si cela vous tente, n'ayez pas peur de pas assez participer, il n'y a aucune participation minimale et toute aide sera la bienvenue :3

Il faut cependant respecter ces principes :

* Le projet se fera en HTML/CSS/JS pour permettre une rétrocomtabilité forte et pour permettre l'usage de toute plateforme
* La branche master correspond à la branche déployé sur le site donnée précédemment : Ainsi, il est impossible pour n'importe qui d'autre que les maiteneurs de merge une branche sur master
* Chacun peut créer librement des branches pour déployer leur fonctionnalité. Le projet étant formé uniquement sur HTML/CSS/JS, il est facile de le tester chez soi
	* Lorsque votre fonctionnalité/ajout/changement est fini, veuillez demander sur gitlab a merge sur master en expliquant brièvement vos ajouts
	* Soyez explicite et n'hésitez pas à faire plusieurs branches pour plusieurs idées
	* Toutes les librairies extérieurs doivent être sous licence libre. Veuillez les préciser si vous en ajoutez sur votre branche et expliquez en soi elle vous serait utile
	* Suivez les issues sur gitlab afin de suivre les tâches à réaliser
* Le projet doit suivre la licence précisé dans LICENSE (licence libre). Toutes vos participations respécterons cette licence

Il faut demander sur gitlab l'accès au projet. Faites le et vous serez accepté dans les 24h selon nos réflexes ^^

## Règles d'or du projet

Hésitez pas à en ajouter aussi lors de vos commits :3

Ce sont les règles, les objectifs que doivent suivre le site internet. En queleque sorte, c'est notre cahier des charges

* Le site doit être érgonomique pour mobile principalement, pour ordinateur aussi idéalement
* Peu de temps doit être passée sur le site internet, il faut que l'utilisateur ait accès rapidement au site internet
* Keep It Simple Stupid : Il faut que le principal soit bien fait, les fonctionnalités secondaires viendrons en arrière plan et sans pertuber l'utilisation principale
